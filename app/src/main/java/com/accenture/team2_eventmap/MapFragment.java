package com.accenture.team2_eventmap;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;


import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;

import com.bumptech.glide.request.target.Target;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;


import java.util.ArrayList;
import java.util.List;

import static com.accenture.team2_eventmap.HomeFragment.progress;


public class MapFragment extends Fragment {
    static SupportMapFragment mapFragment;
    FusedLocationProviderClient client;
    static private Context mContext;
    static View view;

    static boolean notFirst = false,  fromFileDate = false;

    public static ArrayList<events> Data = new ArrayList<>();
    public static ArrayList<events> interestedInList = new ArrayList<>();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    public static void setFromFileDate(boolean file) {
        fromFileDate = file;
    }


    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Inflating view so I can call methods from it
        view = inflater.inflate(R.layout.fragment_map, container, false);



        //Initializing Map and current location
        mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.google_map);
        client = LocationServices.getFusedLocationProviderClient(this.getContext());

        //gets current location
        getCurrentLocation();


        //loads events from file but by dates. When map is reloded it shows all events from file because fromFileDate is changed to false
        if (fromFileDate) MapFragment.getAllEvents(events_load.loadFromFileWithDate(MapFragment.getmContext()));


        return view;
    }


    //Getting current location and showing it on map
    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        MarkerOptions myPositionMarker = new MarkerOptions().position(latLng).title("Mana atrašānās vieta").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_user));
                        //I set custom map style. It can be changed
                        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        googleMap.addMarker(myPositionMarker);
                    }
                });
            }
        });
    }



    //Gets Data array from MainActivity when asyncTask(content) is done and then sends each event with its params to createMarker
    public static void getAllEvents(ArrayList<events> data){
        ArrayList<events> list = data;
        Geocoder geoCoder;
        for(int i=0;i<list.size();i++){
            try {
                geoCoder = new Geocoder(mContext);
                List<Address> addressList;
                addressList = geoCoder.getFromLocationName(list.get(i).location, 1);
                if(addressList.size()==0) continue;

                createMarker(list,
                        list.get(i).location,
                        addressList.get(0).getLatitude(),
                        addressList.get(0).getLongitude(),
                        list.get(i).eventName,
                        list.get(i).date,
                        list.get(i).priceMin,
                        list.get(i).priceMax,
                        list.get(i).description);


            }catch(Exception e){
                System.err.println(e+"----");
            }
        }
        if(fromFileDate){
            progress.dismiss();
            fromFileDate = false;
        }
    }

    //Creates Marker on map
    private static void createMarker(final ArrayList<events> data,final String adress, final double lat, final double lon, final String title, final String date, final String minPrice, final String maxPrice,  final String description){
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {


                //makes Snippet string
                String markerInfo = "Adrese: " + adress +"\n";
                if(description!=null) markerInfo = markerInfo + "\nApraksts: " + description + "\n";

                markerInfo = markerInfo+ "\n"+ "Datums: " +  date;

                if(maxPrice==""){
                    markerInfo = markerInfo + "\nCena: " + minPrice;
                }else markerInfo = markerInfo + "\nCena no: " + minPrice + " līdz: " + maxPrice;



                //marker params
                LatLng latLng = new LatLng(lat,lon);
                 MarkerOptions eventMarker = new MarkerOptions()
                        .position(latLng)
                        .title(title)
                        .snippet(markerInfo);

                //if event is in interested list, then changes its icon
                if(interestedInList.size()!=0){
                    System.out.println(interestedInList.size());
                    for(int j=0;j<interestedInList.size();j++){
                        if(title.equals(interestedInList.get(j).eventName)){
                             eventMarker = new MarkerOptions()
                                    .position(latLng)
                                    .title(title)
                                    .snippet(markerInfo)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_interested_in));
                        }
                    }
                }


                googleMap.setPadding(0,80,0,0);
                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(final Marker marker) {

                        final Dialog popUp = new Dialog(mContext);
                        popUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        popUp.setContentView(R.layout.info_window_pop_up);
                        popUp.setCanceledOnTouchOutside(false);

                        boolean interested = false;

                        Button close = popUp.findViewById(R.id.closeBtn);
                        final Button interstedIn = popUp.findViewById(R.id.interestedInBtn);
                        Button openLink = popUp.findViewById(R.id.openLinkBtn);

                        close.setEnabled(true);
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popUp.cancel();
                            }
                        });

                        //checks if the event is in interested list, if it is, then changes text in button
                        interstedIn.setEnabled(true);
                        for(int i=0;i<interestedInList.size();i++){
                            if(marker.getTitle().equals(interestedInList.get(i).eventName)) interested=true;
                        }
                        if(interested) interstedIn.setText("Noņemt no ieinteresēto saraksta");
                        final boolean finalInterested = interested;
                        interstedIn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try{
                                    //if event is in the list, then it offers to delte it from list and changes the icon
                                    if(finalInterested){
                                        for(int i=0;i<interestedInList.size();i++){
                                            if(marker.getTitle().equals(interestedInList.get(i).eventName)){
                                                for(int j=0;j<data.size();j++){
                                                    if(interestedInList.get(i).eventName.equals(data.get(j).eventName)) data.get(j).interested = false;
                                                }
                                                interestedInList.remove(i);
                                                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                            }

                                        }
                                    }else {
                                        //if event is not in list, then it offers to add it and changes icon
                                        for (int i = 0; i < data.size(); i++) {
                                            if (marker.getTitle().equals(data.get(i).eventName)) {
                                                data.get(i).interested = true;
                                                interestedInList.add(data.get(i));
                                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_interested_in));
                                            }
                                        }
                                    }
                                    if(!notFirst){
                                        notFirst = true;
                                        marker.showInfoWindow();
                                    }
                                    notFirst=false;
                                popUp.cancel();
                                }catch (Exception e){}

                            }
                        });

                        openLink.setEnabled(true);
                        openLink.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Searchers for current marker and opens url
                                for(int i=0; i<data.size();i++){
                                    try {
                                        if (marker.getTitle().equals(data.get(i).eventName)) {
                                            Uri uri = Uri.parse(data.get(i).eventLink.replaceAll("\\s+",""));
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                            mContext.startActivity(intent);
                                        }
                                    }catch (Exception e){}
                                }
                            }
                        });



                        popUp.show();
                    }
                });


                //sets custom marker info window. This can be customized
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(final Marker marker) {
                        final LinearLayout info = new LinearLayout(mContext);
                        info.setOrientation(LinearLayout.VERTICAL);
                        info.setPadding(10,10,10,10);

                        TextView title = new TextView(mContext);
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());
                        info.addView(title);

                        final ImageView img = new ImageView(mContext);
                        for (int i = 0; i < data.size() - 1; i++) {
                            try {
                                if (marker.getTitle().equals(data.get(i).eventName)) {
                                    GlideApp.with(mContext)
                                            .load(data.get(i).eventImage.replaceAll("\\s+",""))
                                            .override(400, 250)
                                            .centerCrop()
                                            .listener(new RequestListener<Drawable>() {
                                                @Override
                                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                    info.addView(img);
                                                    if(!notFirst){
                                                        notFirst = true;
                                                        marker.showInfoWindow();
                                                    }
                                                    return false;
                                                }
                                            })
                                            .into(img);

                                }
                            } catch (Exception e) {
                                System.err.println("img " + e);
                            }
                        }

                        TextView snippet = new TextView(mContext);
                        snippet.setTextColor(Color.GRAY);
                        snippet.setText(marker.getSnippet());

                        TextView link = new TextView(mContext);
                        link.setTextColor(Color.BLUE);

                        //Searchers for current marker and puts url in infoWindow
                        for(int i=0; i<data.size()-1;i++){
                            try {
                                if (marker.getTitle().equals(data.get(i).eventName)) {
                                    link.setText(data.get(i).eventLink.replaceAll("\\s+",""));
                                }
                            }catch (Exception e){}
                        }




                        info.addView(snippet);
                        info.addView(link);
                        notFirst = false;
                        return info;
                    }
                });

                googleMap.addMarker(eventMarker);
            }
        });

    }



    public static Context getmContext(){
        return mContext;
    }


}
