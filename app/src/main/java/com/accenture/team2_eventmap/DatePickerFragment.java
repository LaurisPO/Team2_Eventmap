package com.accenture.team2_eventmap;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import static com.accenture.team2_eventmap.HomeFragment.progress;

import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private Button button;
    private Button button1;
    private FrameLayout fFile;
    private FrameLayout fWeb;

    public static Date dateFrom = Calendar.getInstance().getTime();
    public static Date dateTill = Calendar.getInstance().getTime();
    static Date dateChosen = Calendar.getInstance().getTime();
    static boolean buttonClicked;
    static private Context mContext;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Sets View to fragment_calendar
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        System.out.println(Calendar.getInstance().getTime());

        //Adds all buttons from fragment_calendar
        button = view.findViewById(R.id.button_from);
        button1 = view.findViewById(R.id.button_till);
        fFile = view.findViewById(R.id.fFile);
        fWeb = view.findViewById(R.id.fWeb);
        //Sets Click Listeners to all assigned buttons
        button.setOnClickListener(this);
        button1.setOnClickListener(this);
        fFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //sets that events are read by date from file
                MapFragment.setFromFileDate(true);

                if (!events_load.loadFromFileWithDate(mContext).isEmpty()){
                    //If it is not stable remove and fix navigationView on MainActivity
                    MainActivity.navigationView.setCheckedItem(R.id.nav_map);

                    progress = ProgressDialog.show(mContext, "Paziņojums", "Notiek datu ielāde no faila, tas var aizņemt kādu laiku.", true);

                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MapFragment()).commit();

                }
                else {
                    Toast.makeText(mContext, "Failā nav iekļauti pasākumi attiecīgajos datumos. Nepieciešams lejupielādēt pasākumus no interneta.", Toast.LENGTH_SHORT).show();
                    MainActivity.navigationView.setCheckedItem(R.id.nav_date_picker);
                    progress.dismiss();
            }
            }
        });
        fWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Change menu selected tab
                MainActivity.navigationView.setCheckedItem(R.id.nav_map);
                MapFragment.setFromFileDate(false);
                if(ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

                    progress = ProgressDialog.show(mContext,"Paziņojums", "Notiek pasākumu ielāde no interneta, tas var aizņemt kādu laiku.", true);


                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MapFragment()).commit();
                    //Gets data from bilesuparadize.lv
                    events_bilesuParadize bParadize = new events_bilesuParadize();
                    bParadize.new Content().execute();
                }else{
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},44);
                }
            }
        });

        //Returns assigned view (Shows the fragments_calendar fragment)
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar currenctDate = Calendar.getInstance();
        int year = currenctDate.get(Calendar.YEAR);
        int month = currenctDate.get(Calendar.MONTH);
        int day = currenctDate.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);

        dateChosen = date.getTime();
        Calendar dateCheck = Calendar.getInstance();
        dateCheck.set(2021, 0, 1, 0, 0);

        if (dateChosen.after(dateCheck.getTime())) {
            Toast.makeText(getContext(), "Ievadiet gadu, kas ir līdz 2020 gada", Toast.LENGTH_LONG).show();
        } else {

            if (buttonClicked) {
                dateFrom = dateChosen;
            } else {
                dateTill = dateChosen;
            }
        }
    }


    @Override
    public void onClick(View view) {
            //Opens Calendar that you can choose the date from
            DialogFragment datePicker = new DatePickerFragment();
            datePicker.show(getFragmentManager(), "Date picker");
            if (view.getId() == R.id.button_from) {
                buttonClicked = true;
            } else {
                buttonClicked = false;
            }

    }


    //Asking for permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                progress = ProgressDialog.show(this.getContext(), "Paziņojums", "Notiek datu ielāde no interneta, tas var aizņemt kādu laiku.", true);

                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MapFragment()).commit();
                //Gets data from bilesuparadize.lv
                events_bilesuParadize bParadize = new events_bilesuParadize();
                bParadize.new Content().execute();

            }
        }
    }
}
