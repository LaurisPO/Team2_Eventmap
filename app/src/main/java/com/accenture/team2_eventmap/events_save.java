package com.accenture.team2_eventmap;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class events_save {
    static void saveToFile(ArrayList<events> data, Context context) throws IOException {
        String fPath = context.getFilesDir().getPath().toString() + "/eventList.txt";
        File file = new File(fPath);
        if(!file.exists()) file.createNewFile();
        try{
            FileOutputStream fos = context.openFileOutput("eventList.txt", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(data);
            os.close();
        } catch (Exception e) {
            System.out.println("save" + e);
        }
    }



    static void saveToFileInterested(ArrayList<events> data, Context context) throws IOException {
        String fPath = context.getFilesDir().getPath().toString() + "/eventListInterested.txt";
        File file = new File(fPath);
        if(!file.exists()) file.createNewFile();
        try{
            FileOutputStream fos = context.openFileOutput("eventListInterested.txt", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(data);
            os.close();
        } catch (Exception e) {
            System.out.println("save" + e);
        }
    }

}
