package com.accenture.team2_eventmap;

import androidx.fragment.app.Fragment;

import java.io.Serializable;
import java.util.Date;


public class events extends Fragment implements Serializable {
//Made this so that every array of event we make is the same
    String eventName;
    String date;
    Date date_date;
    String location;
    String priceMin;
    String priceMax;
    String eventLink;
    String eventImage;
    String description;
    Boolean interested;

}
