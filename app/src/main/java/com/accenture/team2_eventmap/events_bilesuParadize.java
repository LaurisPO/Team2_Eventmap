package com.accenture.team2_eventmap;

import android.os.AsyncTask;

import androidx.fragment.app.Fragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import static com.accenture.team2_eventmap.MapFragment.Data;


public class events_bilesuParadize extends Fragment {
    boolean duplicate = false;


    public class Content extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //If List after BilesuParadize is 0, that means there are no events left today
            //Then it makes dateTill +1 day and runs through again.
            //Only then it calls liveRiga events
            if (Data.size() == 0) {
                Date dTill = DatePickerFragment.dateTill;
                DatePickerFragment.dateTill = new Date(dTill.getTime() + (1000 * 60 * 60 * 24));

                events_bilesuParadize bParadize = new events_bilesuParadize();
                bParadize.new Content().execute();
            } else {

                //Gets data from liveRiga.lv
                events_liveRiga lRiga = new events_liveRiga();
                lRiga.new Content().execute();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            events check = new events();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date SystemDate = new Date();
            Date ChosenDate = new Date();
            ChosenDate = DatePickerFragment.dateTill;
            boolean checker = false;

            for (int pageInt = 1; pageInt < 50; pageInt++) {
                try {
                    String url = "https://www.bilesuparadize.lv/lv/search?dateFrom=" + formatter.format(DatePickerFragment.dateFrom) +
                            "&dateTo=" + formatter.format(ChosenDate) + "&page=" + pageInt;
                    Document doc = Jsoup.connect(url).get();
                    Element table = doc.select("table").get(0);
                    Elements rows = table.select("tr");


                    events temp = new events();
                    for (int i = 0; i < rows.size(); i++) {
                        duplicate = false;

                        Element row = rows.get(i);
                        Elements col = row.select("td");

                        Elements urlElement = col.get(0).select("a");
                        String link = "https://www.bilesuparadize.lv" + urlElement.attr("href");
                        temp.eventLink = link;

                        Element imageElement = col.select("img").first();
                        String absoluteUrl = imageElement.absUrl("src");
                        temp.eventImage = absoluteUrl;

                        Elements dateTmp = col.get(2).select("div");
                        String date = dateTmp.select("div").text();

                        String[] arr = date.split(" ", 0);
                        for (int j = 2; j <= 4; j++) {
                            if (j == 2) temp.date = arr[j] + " ";
                            else temp.date = temp.date + arr[j] + " ";
                        }


                        temp.date_date = DateFormater(date);
                        temp.interested = false;

                        Elements nameAndLocation = col.get(3).select("div");
                        String name = nameAndLocation.select("h5").text();
                        String location = nameAndLocation.select("p").text();
                        temp.eventName = name;
                        for(int s=0;s<Data.size();s++){
                            if(Data.get(s).eventName.equals(temp.eventName)){
                                duplicate=true;
                                break;
                            }
                        }
                        if(duplicate) continue;

                        temp.location = location;

                        Elements priceList = col.get(4).select("ul").select("li");
                        Element price = priceList.get(0);
                        String priceMax = price.text();
                        int test = priceList.size();
                        String priceMin = priceList.get(test - 1).text();
                        temp.priceMin = priceMin;
                        temp.priceMax = priceMax;

                        Data.add(temp);
                        temp = new events();
                    }

                } catch (Exception e) {
                    System.err.println(e);
                    return null;
                }
            }
            return null;
        }
    }

    public static Date DateFormater(String date) {
        String BezDienas = "";
        for (int i = 0; i < date.length(); i++) {
            if (date.charAt(i) == ',') {
                BezDienas = date.substring(i + 2);
                break;
            }
        }

        String Diena = "";
        for (int i = 0; i < BezDienas.length(); i++) {
            if (BezDienas.charAt(i) == '.') {
                Diena = BezDienas.substring(0, i);
                BezDienas = BezDienas.substring(i + 2);
                break;
            }
        }
        String Menesis = "";
        for (int i = 0; i < BezDienas.length(); i++) {
            if (BezDienas.charAt(i) == ',') {
                Menesis = BezDienas.substring(0, i);
                break;
            }
        }
        int month = 0;
        switch (Menesis) {
            case "janvāris":
                month = 1;
                break;
            case "februāris":
                month = 2;
                break;
            case "marts":
                month = 3;
                break;
            case "aprīlis":
                month = 4;
                break;
            case "maijs":
                month = 5;
                break;
            case "jūnijs":
                month = 6;
                break;
            case "jūlijs":
                month = 7;
                break;
            case "augusts":
                month = 8;
                break;
            case "septembris":
                month = 9;
                break;
            case "oktobris":
                month = 10;
                break;
            case "novembris":
                month = 11;
                break;
            case "decembris":
                month = 12;
                break;
        }

        Date SystemDate = new Date();
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.set(2020, month - 1, Integer.valueOf(Diena));
        SystemDate = dateCalendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM");
        return SystemDate;
    }

}