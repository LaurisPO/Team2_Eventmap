package com.accenture.team2_eventmap;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class GiftFragment extends Fragment{


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gifting,container,false );

        view.findViewById(R.id.redirectToWEB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickButton("https://www.bilesuparadize.lv/lv/gift-cards");

            }
        });



        return view;
    }
    public void clickButton(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
