package com.accenture.team2_eventmap;

import android.os.AsyncTask;

import androidx.fragment.app.Fragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.accenture.team2_eventmap.MapFragment.Data;
import static com.accenture.team2_eventmap.HomeFragment.progress;

public class events_liveRiga extends Fragment{

    boolean duplicate = false;

    public class Content extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                System.out.println("pabeidza scrapot");
                //Saving events
                events_save.saveToFile(Data,MapFragment.getmContext());
                //Adding events from liveRiga to map
                MapFragment.getAllEvents(events_load.loadFromFileWithDate(MapFragment.getmContext()));

                progress.dismiss();
                System.out.println("pabeidza likt uz kartes");
            } catch (IOException e) {
                e.printStackTrace();
            }




        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String url = "https://www.liveriga.com/lv/3-pasakumi?page=";
            String newUrl = null;
            String date = null;

            for (int j = 1; j <= 2; j++) {
                newUrl = url + String.valueOf(j);
                duplicate = false;

                try {
                    Document doc = Jsoup.connect(newUrl).get();
                    Element table = doc.getElementsByClass("activity-list lr-row").get(0);
                    Elements rows = table.select("div");

                    events tmp = new events();
                    for (int i = 1; i < rows.size(); i++) { ;
                        Element row = rows.get(i);
                        tmp.eventName = row.getElementsByClass("eb_heading eleb_heading").text();
                        for(int s=0;s<Data.size();s++){
                            if(Data.get(s).eventName.equals(tmp.eventName)){
                                duplicate=true;
                                break;
                            }
                        }
                        if(duplicate) continue;
                        Element link = row.select("a").first();
                        date= row.getElementsByClass("event-date").text();
                        tmp.date = date;
                        tmp.date_date = DateFormater(date);
                        Element imageLink = row.select("img").first();
                        tmp.eventLink = link.attr("abs:href");
                        tmp.eventImage = imageLink.attr("abs:src");

                        if (link != null) {
                            try {
                                String eventUrl = link.attr("abs:href");
                                Document eventDoc = Jsoup.connect(eventUrl).get();
                                tmp.priceMin = eventDoc.getElementsByClass("info-price").text();

                                int o = tmp.priceMin.indexOf(' ');
                                String fWord="";
                                String rWord="";
                                if(o>0) {
                                    fWord = tmp.priceMin.substring(0, o);
                                    rWord = tmp.priceMin.substring(o);
                                }

                                if(fWord.equals("Biļetes:")) tmp.priceMin=rWord;

                                tmp.priceMax = "";
                                tmp.interested = false;
                                Element dateClass = eventDoc.getElementsByClass("info-contacts").get(0);
                                Elements dateRow = dateClass.select("dd");
                                tmp.location = dateRow.get(1).text();
                                tmp.description = eventDoc.getElementsByClass("intro").text();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Data.add(tmp);
                        tmp = new events();


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            return null;
        }
    }

    public static Date DateFormater(String date){
        String dataString = date.substring(0, 6);
        String Diena = "";
        for(int i = 0; i < dataString.length(); i++){
            if(date.charAt(i) == ' '){
                Diena = dataString.substring(0,i);
                dataString = dataString.substring(i+1);
                break;
            }

        }
        int month = 0;
        switch(dataString){
            case "Jan":
                month = 1;
                break;
            case "Feb":
                month = 2;
                break;
            case "Mar":
                month = 3;
                break;
            case "Apr":
                month = 4;
                break;
            case "Mai":
                month = 5;
                break;
            case "Jūn":
                month = 6;
                break;
            case "Jūl":
                month = 7;
                break;
            case "Aug":
                month = 8;
                break;
            case "Sep":
                month = 9;
                break;
            case "Okt":
                month = 10;
                break;
            case "Nov":
                month = 11;
                break;
            case "Dec":
                month = 12;
                break;
        }
        Date SystemDate = new Date();
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.set(2020, month-1, Integer.valueOf(Diena));
        SystemDate = dateCalendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM");
        return SystemDate;

    }

}
