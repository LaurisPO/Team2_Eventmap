package com.accenture.team2_eventmap;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<events> dataSet;
    private Context mContext;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class MyViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        RecyclerView mRecyclerView;
        public TextView textViewName;
        public TextView textViewDate;
        public TextView textViewPrice;
        public ImageView star;
        public CardView cView;
        public MyViewHolder(View v) {
            super(v);
            this.textViewName = (TextView) itemView.findViewById(R.id.list_name);
            this.textViewDate = (TextView) itemView.findViewById(R.id.list_date);
            this.textViewPrice = (TextView) itemView.findViewById(R.id.list_price);
            this.cView = itemView.findViewById(R.id.card_pertanyaan);
            this.star = itemView.findViewById(R.id.star);

        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<events> events,Context context) {
        dataSet = events;
        mContext = context;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent,
                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);


        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        TextView textViewName = holder.textViewName;
        TextView textViewDate = holder.textViewDate;
        TextView textViewPrice = holder.textViewPrice;
        ImageView star = holder.star;

        System.out.println("++++++ Izpidās: " + position + " +++++++");

        holder.cView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)mContext).openLink(position);
            }
        });

        if(dataSet.get(position).interested){
                star.setVisibility(View.VISIBLE);
        }else star.setVisibility(View.GONE);

        textViewName.setText(dataSet.get(position).eventName);
        textViewDate.setText(dataSet.get(position).date);
        textViewPrice.setText("Cena: "+dataSet.get(position).priceMin);



        System.out.println("^^^^^^ Izpidās: " + position + " ^^^^^^^");

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        System.out.println("++++++ Size: " + dataSet.size() + " +++++++");
        return dataSet.size();
    }


}
