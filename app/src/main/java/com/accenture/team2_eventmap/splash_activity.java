package com.accenture.team2_eventmap;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class splash_activity extends AppCompatActivity {

    private static int SPLASH_SCREEN_TIMEOUT = 4000;
    Animation topAnimation, botAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


        //Puts app to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //sets xml view
        setContentView(R.layout.splash_activity);

        //animations settings
        Animation fadeOUT = new AlphaAnimation(1, 0);
        fadeOUT.setInterpolator(new AccelerateInterpolator());
        fadeOUT.setStartOffset(1000);
        fadeOUT.setDuration(3000);

        ImageView image = findViewById(R.id.splash);
        image.setAnimation(fadeOUT);

        TextView name = findViewById(R.id.SplashText1);
        name.setAnimation(fadeOUT);

        TextView name2 = findViewById(R.id.SplashText2);
        name2.setAnimation(fadeOUT);

        topAnimation = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        botAnimation = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        image.setAnimation(topAnimation);
        name.setAnimation(botAnimation);
        name2.setAnimation(botAnimation);




        //Runs Main activity after animations is done
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){
                Intent intent = new Intent(splash_activity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        }, SPLASH_SCREEN_TIMEOUT);

    }


}