package com.accenture.team2_eventmap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import static com.accenture.team2_eventmap.HomeFragment.progress;
import static com.accenture.team2_eventmap.MapFragment.Data;
import static com.accenture.team2_eventmap.MapFragment.interestedInList;

import com.google.android.material.navigation.NavigationView;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    public static NavigationView navigationView;
    private View decorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        if (visibility == 0) {
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                    }
                });



        //Menu bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Removes toolbar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Menu bar button
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //If menu item is unselected (also when app is started) it defaults to map(can be change to something else
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }


    }

    @Override
    public void onPause(){
        super.onPause();
        try {
            events_save.saveToFileInterested(interestedInList, this);
            System.out.println("intersted : pause");
        }catch (Exception e){}
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            interestedInList=events_load.loadFromFileInteresed(this);
            System.out.println("intersted : resume");
        }catch (Exception e){}
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.nav_map:
                progress = ProgressDialog.show(this,"Paziņojums", "Notiek datu ielāde no faila, tas var aizņemt kādu laiku.", true);
                DatePickerFragment.dateFrom = Calendar.getInstance().getTime();
                DatePickerFragment.dateTill = Calendar.getInstance().getTime();
                MapFragment.setFromFileDate(true);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MapFragment()).commit();
                break;
            case R.id.nav_list:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ListFragment()).commit();
                break;
            case R.id.nav_date_picker:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DatePickerFragment()).commit();
                break;
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HomeFragment()).commit();
                break;
            case R.id.nav_gifting:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new GiftFragment()).commit();

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Menu button close/open
    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

    public void openLink(int pos){
        Uri uri = Uri.parse(ListFragment.myDataset.get(pos).eventLink.replaceAll("\\s+",""));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


}
