package com.accenture.team2_eventmap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListFragment extends Fragment {
    private RecyclerView recyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    boolean check = false;
    static ArrayList<events> myDataset = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_full);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        if(events_load.loadFromFileWithDate(getContext()).size()==0)  Toast.makeText(getContext(),"Fails nav izveidots. Nepieciešams lejupielādēt pasākumus no interneta.",Toast.LENGTH_SHORT).show();
        else {
            System.out.println("aizgāja");
            ArrayList<events> tmp = events_load.loadFromFileWithDate(getContext());
            ArrayList<events> tmp1 = new ArrayList<>();
            System.out.println(tmp.size());

            if(MapFragment.interestedInList.size()!=0){
                for(int i=0;i<MapFragment.interestedInList.size();i++){
                    myDataset.add(MapFragment.interestedInList.get(i));
                }
                for (int i = 0; i<tmp.size();i++){
                    for(int j=0;j<myDataset.size();j++){
                        if(myDataset.get(j).eventName.equals(tmp.get(i).eventName)) check = true;
                    }
                    if(!check) tmp1.add(tmp.get(i));
                    check = false;
                }
                for(int s = 0;s<tmp1.size();s++){
                    myDataset.add(tmp1.get(s));
                    System.out.println(MapFragment.interestedInList.size());
                }
            }else{
                myDataset = events_load.loadFromFile(getContext());
            }



            mAdapter = new MyAdapter(myDataset,getContext());
            recyclerView.setAdapter(mAdapter);
        }


        return view;
    }


}
