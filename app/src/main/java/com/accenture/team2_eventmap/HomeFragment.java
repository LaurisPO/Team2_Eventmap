package com.accenture.team2_eventmap;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import java.util.Calendar;

public class HomeFragment extends Fragment {
    static private Context mContext;
    FrameLayout fWeb;
    FrameLayout fFile;
    public static ProgressDialog progress;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //User chooses to load events form net and checks permissions
        fWeb = view.findViewById(R.id.loadFromWebFrame);
        fWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapFragment.setFromFileDate(false);
                if(ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                    //Change menu selected tab
                    MainActivity.navigationView.setCheckedItem(R.id.nav_map);


                    progress = ProgressDialog.show(mContext,"Paziņojums", "Notiek pasākumu ielāde no interneta, tas var aizņemt kādu laiku.", true);

                    //When calling from home, or reloding map, date from and date till is set to today
                    DatePickerFragment.dateFrom = Calendar.getInstance().getTime();
                    DatePickerFragment.dateTill = Calendar.getInstance().getTime();


                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MapFragment()).commit();
                    //Gets data from bilesuparadize.lv
                    events_bilesuParadize bParadize = new events_bilesuParadize();
                    bParadize.new Content().execute();
                }else{
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},44);
                }
            }
        });


        //Users chooses to load events from file. When app is opened the first time, file does not exist and it gives msg to user.
        //If file exist and there is events it map
        fFile = view.findViewById(R.id.loadFromFileFrame);
        fFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if this boolean is true, then when map is loaded it will get events from file
                MapFragment.setFromFileDate(true);
                if(!events_load.loadFromFileWithDate(mContext).isEmpty()){
                    //Change menu selected tab
                    MainActivity.navigationView.setCheckedItem(R.id.nav_map);


                    progress = ProgressDialog.show(mContext,"Paziņojums", "Notiek datu ielāde no faila, tas var aizņemt kādu laiku.", true);

                    //When calling from home, or reloding map, date from and date till is set to today
                    DatePickerFragment.dateFrom = Calendar.getInstance().getTime();
                    DatePickerFragment.dateTill = Calendar.getInstance().getTime();


                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MapFragment()).commit();

                }
                else {
                    Toast.makeText(mContext,"Fails nav izveidots. Nepieciešams lejupielādēt pasākumus no interneta.",Toast.LENGTH_SHORT).show();
                    MainActivity.navigationView.setCheckedItem(R.id.nav_home);
                }
            }
        });

        return view;
    }


    //Asking for permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(requestCode==44){
            if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                MainActivity.navigationView.setCheckedItem(R.id.nav_map);

                progress = ProgressDialog.show(mContext,"Paziņojums", "Notiek pasākumu ielāde no interneta, tas var aizņemt kādu laiku.", true);

                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MapFragment()).commit();
                    //Gets data from bilesuparadize.lv
                    events_bilesuParadize bParadize = new events_bilesuParadize();
                    bParadize.new Content().execute();

            }
        }
    }

}
