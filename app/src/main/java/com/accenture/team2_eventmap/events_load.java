package com.accenture.team2_eventmap;

import android.content.Context;


import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class events_load {
    static ArrayList<events> loadFromFile(Context context) {
        ArrayList <events> data = new ArrayList<>();
        try {
            FileInputStream fos = context.openFileInput("eventList.txt");
            ObjectInputStream os = new ObjectInputStream(fos);
            data =(ArrayList<events>) os.readObject();
            os.close();
        } catch (Exception e) {
            System.out.println("load " + e);
        }
        return data;
    }

    static ArrayList<events> loadFromFileWithDate(Context context) {
        ArrayList <events> data = new ArrayList<>();
        try {
            FileInputStream fos = context.openFileInput("eventList.txt");
            ObjectInputStream os = new ObjectInputStream(fos);
            data =(ArrayList<events>) os.readObject();
            os.close();
        } catch (Exception e) {
            System.out.println("load " + e);
        }
        ArrayList<events> dataDates = new ArrayList<>();

        Calendar calFrom = Calendar.getInstance();
        calFrom.setTime(DatePickerFragment.dateFrom);
        calFrom.set(Calendar.HOUR_OF_DAY,0);
        calFrom.set(Calendar.MINUTE,1);
        calFrom.set(Calendar.SECOND,0);
        calFrom.set(Calendar.MILLISECOND,0);

        Calendar calTill = Calendar.getInstance();
        calTill.setTime(DatePickerFragment.dateTill);
        calTill.set(Calendar.HOUR_OF_DAY,23);
        calTill.set(Calendar.MINUTE,59);
        calTill.set(Calendar.SECOND,0);
        calTill.set(Calendar.MILLISECOND,0);

        System.out.println("========LOAD============" + calFrom.getTime());
        System.out.println("========LOAD============" + calTill.getTime());
        for(int i = 0; i < data.size(); i++){
            Date test = data.get(i).date_date;
            System.out.println("================" + test);
            if(test != null && test.after(calFrom.getTime()) &&
                    test.before(calTill.getTime()) ){
                dataDates.add(data.get(i));
            }
        }

        return dataDates;
    }



    static ArrayList<events> loadFromFileInteresed(Context context) {
        ArrayList <events> data = new ArrayList<>();
        try {
            FileInputStream fos = context.openFileInput("eventListInterested.txt");
            ObjectInputStream os = new ObjectInputStream(fos);
            data =(ArrayList<events>) os.readObject();
            os.close();
        } catch (Exception e) {
            System.out.println("load " + e);
        }
        return data;
    }

}
