# 
Events Latvia
#
github - Team2_Eventmap
#
Events Latvia is partially implemented application, the main purpose of this app is to find all the 
upcoming events in the city you are currently located at. Basically listing all the necessary information
about events in one place(Event type, name, location, price, description)
#
In case you want to participate and update this project, make sure :

*You have the latest version of Android Studio on your machine.
*You have basic knowledge of jsoup library.
*You are interested in the applications topic itself.

#
- TO install application please download the .apk file from - "https://www.dropbox.com/s/5d213gy9kq3qkjb/app-debug.apk?dl=0"
- Install application (You have to accept necessary permissions to install this .apk on your device)
- Open application.
- See the features available in application( Main screen, Map view, List view, Date pick, and Gift aisle)
#
TO contribute in this project :

- Open Android Studio
- Choose to get the project through Version Control
- Aisle asking for URL will popup - URL : https://github.com/Kristericc/Team2_Eventmap.git
- Please examine, and understand the each file and their usage in project, before making any changes.

* You can also download project archive file, and import it into the Android Studio.

#
CONTRIBUTORS:
All the necessary infromation about project contributors is freely available in github page.
In order to ask questions, use Github communication options with previous contributors.


